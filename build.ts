import * as path from 'path';
import * as fs from 'fs';
import * as _glob from 'glob';
import * as rimraf from 'rimraf';
import * as util from 'util';
import { BabelFileResult, transformFile as _transformFile, TransformOptions } from '@babel/core';

const BUILD_DIR = 'build';
const DIST_DIR = 'dist';
const ENTRY= `${BUILD_DIR}/main-compiled.js`;
const SOURCE_MAP_COMMENT = '\n//# sourceMappingURL=';

const glob = util.promisify(_glob);
const mkdir = util.promisify(fs.mkdir);
const transformFile: (path: string, opts?: TransformOptions) => Promise<BabelFileResult> = util.promisify(_transformFile);
const writeFile = util.promisify(fs.writeFile);

main().then(() => console.log('Build complete.'));

async function main() {
    await build(ENTRY, {
        buildPath: BUILD_DIR,
        distPath: DIST_DIR
    });
}

interface BuildOptions {
    buildPath: string;
    distPath: string;
}

async function build(
    entry: string,
    options: BuildOptions
): Promise<void> {
    // Remove files in dist
    console.log(`Removing all files in ${options.distPath}...`);
    await util.promisify(rimraf)(`${options.distPath}/*`);

    const dirList = await glob(`${options.buildPath}/**/*.js`);
    console.log(`Transpiling ${dirList.length} files...`);
    for (const filePath of dirList) {
        const parts = filePath.split('.');
        if ('js' !== parts.pop()) {
            continue;
        }
        const baseFileName = path.basename(parts.join('.'));
        const baseDistPath = path.dirname(`${options.distPath}${filePath.slice(options.buildPath.length)}`);

        if (!fs.existsSync(baseDistPath)) {
            await mkdir(baseDistPath);
        }

        const result = await transformFile(filePath);
        let code = result.code;
        if (result.map) {
            result.map.file = `${baseFileName}.js`;
            const mapFile = `${baseFileName}.js.map`;
            await writeFile(`${baseDistPath}/${mapFile}`, JSON.stringify(result.map));
            code += `${SOURCE_MAP_COMMENT}${mapFile}`;
        }
        if (result.code) {
            await writeFile(`${baseDistPath}/${baseFileName}.js`, code);
        }
    }
    console.log(`Transpiling finished.`);
}
